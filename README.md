# 介绍

这是一些我在看[Evolutionary Dynamics: Exploring the Equations of Life](http://www.amazon.com/Evolutionary-Dynamics-Exploring-Equations-Life/dp/0674023382?ie=UTF8&camp=1789&creative=9325&linkCode=ur2&tag=v-i-20)这本书时重复书中的一些模型所写的脚本。

脚本主要由Python写成。运行黄精Python2.7，需要Numpy和Matplotlib的支持。

# logistic.py

画出了 Logistic 映射的分岔图。
