# -*- coding: utf8 -*-
import numpy as np
import matplotlib.pyplot as plt


def logistic(r, x0):
    while True:
        x = r * x0 * (1 - x0)
        yield [r, x]
        x0 = x

times = 200
result = []
for r in np.linspace(2.1, 4, 200):
    gen = logistic(r, 0.5)
    for i in range(times):
        result.append(next(gen))
logistic1 = np.asarray(result)

fig2, ax2 = plt.subplots()
fig2.set_size_inches(10, 8)
ax2.set_xlim([2.1, 4])
ax2.set_xlabel("a")
ax2.set_ylabel("x")
ax2.scatter(logistic1[100:, 0], logistic1[100:, 1], s=1)

plt.show()
